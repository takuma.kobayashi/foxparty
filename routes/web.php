<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// 参加者側
// 選手選択画面
Route::get('/select/{team}',     'FoxPartyController@select');
// 投票の結果をDBに登録
Route::post('/selected',         'FoxPartyController@selected');

// 主催側
// チームの投票結果を表示
Route::get('/team_select',       'FoxPartyController@team_select');
// チームのトータルポイントを表示
Route::get('/team_point',        'FoxPartyController@team_point');
// 試合結果を受け取りDBにポイントを加算
Route::get('/result',            'FoxPartyController@result');
Route::post('/point_get',        'FoxPartyController@point_get');