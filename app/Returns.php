<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Returns extends Model
{
	protected $table = "returns";
	const CREATED_AT = 'return_at';
	const UPDATED_AT = 'return_at';
	//$timestamps = false;
} 