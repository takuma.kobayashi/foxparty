<?php

namespace App\Http\Controllers;
use App\books;
// use App\Rentals;
// use App\Returns;
use App\Rental_log;
use App\Users;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book = Books::all();
        $userId = Auth::id();
        $rent = Rental_log::all();

        foreach($rent as $value){
            if($value ->return_at === null && $userId !== $value ->user_id){
                $bookId = $value ->book_id;
                foreach ($book as $value) {
                    if($bookId === $value ->id){
                        $value ->status = 2;
                    }
                }
            }
        }
        // var_dump($book);
        // exit;
        foreach ($book as $key => $value) {
            $arr["id"] = $value ->id;
            $arr["title"] = $value ->title;
            $arr["status"] = $value ->status;
            $array[] = $arr;
        }
        
        return view('auth.booklist',['books'=>$array]);
    }
    public function borrow()
    {
        $data = Books::find($_GET['id']);
        $arr = [];
        $arr['id'] = $data->id;
        $arr['status'] = $data->status;
        $arr['title'] = $data->title;
        
        return view('auth.borrow',$arr);

    }
    public function create()
    {
        $data = Books::find($_GET['id']);
        $rent = new Rental_log;
        $userId = Auth::id();
        $rent ->user_id = $userId;
        $rent ->book_id = $data ->id;
        $rent ->return_date = $_GET['return_date'];
        $rent ->save();
        $data ->status = $_GET['status'];
        $data->save();
         return redirect()-> action('HomeController@index');
    }
    public function return()
    {
        $data = Books::find($_GET['id']);
        $data ->status = $_GET['status'];
        $data ->save();
        $return = Rental_log::orderby('id', 'desc')
        -> where('book_id', $_GET['id'])
        -> where('user_id', Auth::id())
        -> where('return_at', NULL)
        -> limit(1)
        -> get();
        $return[0] ->return_at = date("Y/m/d H:i:s");
        $return[0] ->save();
        // $return = new Returns;
        // $userId = Auth::id();
        // $return ->user_id = $userId;
        // $return ->book_id = $data ->id;
        // $return ->save();

         return redirect()-> action('HomeController@index');
    }
    public function logout(){
        Auth::logout();
        return redirect()-> action('HomeController@index');
    }

}
