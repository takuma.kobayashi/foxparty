<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FoxPartyController
{

	public function index()
  {
    return view('home');
  }

  public function select($team)
  {
    $name = $team;
    return view('select', compact('name'));
  }

  public function selected (Request $request)
  {
    // リクエスト結果をDBに登録する
    $team = $request->input('name');
    DB::table('teams')
            ->where('name', $request->input('name'))
            ->update(['status' => $request->input('select_no')]);

    return redirect('/select/'.$team);
  }

  public function team_select()
  {
    $select = [];
    $get = DB::table('teams')->select('status')->get();
    foreach ($get as $key => $value) {
      if ($value->status == 1)
      {
        $select[$key]['status'] = "斎藤";
        $select[$key]['color'] = "bg green darken-1";
      }
      elseif ($value->status == 2)
      {
        $select[$key]['status'] = "山本（大）";
        $select[$key]['color'] = "bg orange darken-1";
      }
      elseif ($value->status == 3)
      {
        $select[$key]['status'] = "村上";
        $select[$key]['color'] = "bg blue darken-1";
      }
      elseif ($value->status == 4)
      {
        $select[$key]['status'] = "山本（直）";
        $select[$key]['color'] = "bg red darken-1";
      }else{
        $select[$key]['status'] = "";
        $select[$key]['color'] = "";
      }
    }

    return view('team', compact('select'));
  }

  public function team_point()
  {
    $point = [];
    $get = DB::table('teams')->select('name','point')->get();
    foreach ($get as $key => $value) {
      $point[$key] = $value;
    }
    return view('point',compact('point'));
  }

  public function result()
  {
    return view('result');
  }
  public function point_get(Request $request)
  {
    $winer  = 0;
    $get    = 0;
    $winner = $request->input('winner');
    $get    = $request->input('get');
    DB::table('teams')->where('status', $winner)->increment('point',$get);
    return redirect('/result');
  }
}