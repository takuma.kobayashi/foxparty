<!DOCTYPE html>
<html>
<head>
  <title>チーム表 ポイント</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

</head>
<body>
  <div class="section"></div>
  <!-- チームごとに投票した選手を表示する表 -->
  <div class="container">
    <div class="row">
      <div class="col s12 m3">
        <div class="card">
          <div class="card-title center-align">TEAM-A</div>
          <h5 class="fighter-name card-content center-align bg orange darken-1 white-text">
            <!-- DBから投票した選手名を表示($fighter的な) -->{{$point[0]->point}} pt
          </h5>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card">
          <div class="card-title center-align">TEAM-B</div>
          <h5 class="fighter-name card-content center-align bg grey darken-4 white-text">
            <!-- DBから投票した選手名を表示($fighter的な) -->{{$point[1]->point}} pt
          </h5>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card">
          <div class="card-title center-align">TEAM-C</div>
          <h5 class="fighter-name card-content center-align bg orange darken-1 white-text">
            <!-- DBから投票した選手名を表示($fighter的な) -->{{$point[2]->point}} pt
          </h5>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card">
          <div class="card-title center-align">TEAM-D</div>
          <h5 class="fighter-name card-content center-align bg grey darken-4 white-text">
            <!-- DBから投票した選手名を表示($fighter的な) -->{{$point[3]->point}} pt
          </h5>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col s12 m3">
        <div class="card">
          <div class="card-title center-align">TEAM-E</div>
          <h5 class="fighter-name card-content center-align bg grey darken-4 white-text">
            <!-- DBから投票した選手名を表示($fighter的な) -->{{$point[4]->point}} pt
          </h5>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card">
          <div class="card-title center-align">TEAM-F</div>
          <h5 class="fighter-name card-content center-align bg orange darken-1 white-text">
            <!-- DBから投票した選手名を表示($fighter的な) -->{{$point[5]->point}} pt
          </h5>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card">
          <div class="card-title center-align">TEAM-G</div>
          <h5 class="fighter-name card-content center-align bg grey darken-4 white-text">
            <!-- DBから投票した選手名を表示($fighter的な) -->{{$point[6]->point}} pt
          </h5>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card">
          <div class="card-title center-align">TEAM-H</div>
          <h5 class="fighter-name card-content center-align bg orange darken-1 white-text">
            <!-- DBから投票した選手名を表示($fighter的な) -->{{$point[7]->point}} pt
          </h5>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col s12 m3">
        <div class="card">
          <div class="card-title center-align">TEAM-I</div>
          <h5 class="fighter-name card-content center-align bg orange darken-1 white-text">
            <!-- DBから投票した選手名を表示($fighter的な) -->{{$point[8]->point}} pt
          </h5>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card">
          <div class="card-title center-align">TEAM-J</div>
          <h5 class="fighter-name card-content center-align bg grey darken-4 white-text">
            <!-- DBから投票した選手名を表示($fighter的な) -->{{$point[9]->point}} pt
          </h5>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card">
          <div class="card-title center-align">TEAM-K</div>
          <h5 class="fighter-name card-content center-align bg orange darken-1 white-text">
            <!-- DBから投票した選手名を表示($fighter的な) -->{{$point[10]->point}} pt
          </h5>
        </div>
      </div>
      <div class="col s12 m3">
        <div class="card">
          <div class="card-title center-align">TEAM-L</div>
          <h5 class="fighter-name card-content center-align bg grey darken-4 white-text">
            <!-- DBから投票した選手名を表示($fighter的な) -->{{$point[11]->point}} pt
          </h5>
        </div>
      </div>
    </div>
  </div>
  <!-- 投票が一番少なかった人がオッズ二倍 -->
  <input type="hidden" id="double" value="">
  <!-- 結果を入力で正解チーム色付け -->
  <input type="hidden" id="result" value="">
  <script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>
  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
            
</body>
</html>