<!DOCTYPE html>
<html>
<head>
  <title>チームポイント</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        
</head>
<body>
  <!-- チームごとにトータルポイントを表示する表 -->
  <div class="section"></div>
  <div class="container">
    <form id="point_get" action="/foxparty/public/point_get" method="POST">
      {{ csrf_field() }}
      <div class="row">
        <div class="col m6">
          <select class="browser-default" name="winner">
            <option value="1">斎藤</option>
            <option value="2">山本(大)</option>
            <option value="3">村上</option>
            <option value="4">山本(直)</option>
          </select>
        </div>
        <div class="col m3">ポイント</div>
        <div class="col m3">
          <select class="browser-default" name="get">
            <option value="10">10 pt</option>
            <option value="20">20 pt</option>
          </select>
        </div>
      </div>
      <button class="waves-effect waves-light btn">ポイントゲット！</button>
    </form>
  </div>
  <script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>
  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
            
</body>
</html>