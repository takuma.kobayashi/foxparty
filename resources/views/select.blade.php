<!DOCTYPE html>
<html>
<head>
  <title>投票BOX</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        
</head>
<body>
  <div class="section"></div>
  <div class="container">
    <form id="selecter" action="/foxparty/public/selected" method="POST">
      {{ csrf_field() }}
      <!-- token発行する -->
        <p>
          <label>
            <input class="with-gap" name="select_no" type="radio" value="1">
            <span>斎藤　輝幸</span>
          </label>
        </p>
        <p>
          <label>
            <input class="with-gap" name="select_no" type="radio" value="2">
            <span>山本　ダイキ</span>
          </label>
        </p>
        <p>
          <label>
            <input class="with-gap" name="select_no" type="radio" value="3">
            <span>村上　徹</span>
          </label>
        </p>
        <p>
          <label>
            <input class="with-gap" name="select_no" type="radio" value="4">
            <span>山本　直哉</span>
          </label>
        </p>
      <!-- コントローラからチームの情報受け取ってをバリューに埋め込む -->
      <button class="waves-effect waves-light btn" value="">投票！</button>
      <input type="hidden" name="name" value="{{$name}}">
    </form>
  </div>
  <script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>
  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  <script>
    $(function(){
      $('#selecter').validate({
        rules: {
          fighter_no: {
            required: true,
            minlength: 1
          }
        }
      });
    });
  </script>
</body>
</html>